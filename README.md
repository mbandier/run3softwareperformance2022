# Run3SoftwarePerformance2022

A collection of useful scripts to be used in the Run 3 benchmarking for ACAT 2022.

## Getting started

To begin with, simply clone this repository:

```
git clone https://gitlab.cern.ch/amete/run3softwareperformance2022.git
```

## Explaining the workflow

There are currently three `python` files:

- `definitions.py` contains the common parameters, e.g. location for the results etc.
- `executer.py` is the main file that executes the jobs
- `summarizer.py` is the main file that summarizes the results for the executed jobs

In order to add a new test, you can create a new bash script in the `scripts/` folder.
You can take a look at the existing script, called `data18_RAWtoALL.sh`, to get an idea.
The important aspects here are that:

- The script should be called `${JOB}.sh` (see below)
- The script accepts two arguments:
  - `${1}` : The number of threads
  - `${2}` : The number of events
- The return code of the job trasform is written into a file named `__exitcode`
- `__start` and `__done` can also be written out but not currently used

Then the job can be executed through `executer.py` as follows:

```
executer.py -j ${JOB} -e ${NEVENTS} -t ${NTHREADS}
```

This will execute the job and copy the necessary results under the folder defined
in `definitions.py` (i.e. the user can change these as necessary).

The idea is to run the jobs that scan various numbers of threads through this script.
Then the results from those jobs can be summarized in a single CSV file with the command:

```
summarizer.py -j ${JOB}
```

This will create a simple CSV file (named `${JOB}.summary.csv` under the results folder)
that looks something like:

```
#job,threads,events,evtPerSec,pssPeak
data18_RAWtoALL,4,100,0.520,6.29
```

Then we can use this file to produce the final plots/tables as necessary

A preliminary plotting script is available and can be run as:

```
plot_maker.py -j JOB1,JOB2,JOB3...
```

This will attempt to overlay the results of the comma-separated list of jobs on a single
reasonably attractive ROOT plot. A handful of customization options are available, but it
is likely that some significant customization will be required for final paper plots.

## Guidelines

The envisioned workflow is as follows:
- Fork the repository
- Apply your changes in a dedicated branch
- Submit an MR (master)

We can eventually set up a CI worklow that produces the plots etc. automatically.
