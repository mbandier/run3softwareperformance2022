#!/usr/bin/env python3
''' Main script that summarizes job results '''

from definitions import base_run_dir, base_results_dir
import argparse
import glob
import json
import os
import tarfile

# Summarize the job
def summarize_job( job = 'data18_RAWtoALL' ): 

    # Print some information
    print( f'Processing job {job}' )

    # Set the full results directories
    results = glob.glob( f'{base_results_dir}/{job}-*/perfmonmt_*' )

    # Check to see if we have any results to process
    if len(results) == 0:
        raise RuntimeError( f'No results are found for job {job}!' )

    # Now read the results and summarize them in a csv file
    output_filename = f'{base_results_dir}/{job}.summary.csv'
    if os.path.exists( output_filename ):
        raise RuntimeError( f'Summary output already exists for job {job}, quitting!' )

    with open( output_filename, 'w' ) as output_file:
        # Write the header
        output_file.write( '#job,threads,events,evtPerSec,pssPeak\n' )
        # Now loop over all results and extract the information
        for result in results:
            nthreads = int(result.split('/')[-2].split('-')[1])
            with tarfile.open(result) as tf:
                for member in tf.getmembers():
                    f = tf.extractfile(member)
                    data = json.load(f)

                    # Extract some interesting data
                    nEvents   = int( data["summary"]["nEvents"] )
                    pssPeak   = float( data["summary"]["peaks"]["pssPeak"] )/1024./1024.
                    execWall  = float( data["summary"]["snapshotLevel"]["Execute"]["dWall"] )
                    evtPerSec = float( nEvents-1 )/execWall*1000.
                    
                    output_file.write( f'{job},{nthreads},{nEvents},{evtPerSec:.3f},{pssPeak:.2f}\n' )


# Define the main function
def main():

    # Setup the job parameters here...
    parser = argparse.ArgumentParser( description = 'Basic Result Summarizer',
                                      add_help = True )

    parser.add_argument( '-j', '--job', action = 'store',
                         dest = 'job', default = 'data18_RAWtoALL',
                         help = 'The job that will be executed (default: data18_RAWtoALL)' )
    parser.add_argument( '-v', '--version', action = 'version', version = '%(prog)s 0.1' )

    args = parser.parse_args()

    # Execute the job
    summarize_job( job = args.job )

# When running this as a script
if '__main__' in __name__:
    main()
