#!/bin/bash

# ${1} = Number of threads
# ${2} = Number of events

touch __start;

TRF_ECHO=1 ATHENA_CORE_NUMBER="${1}" Reco_tf.py \
  --inputBSFile "/data/amete/scratch/run3-performance-study-inputs/data18_13TeV/data18_13TeV.00357750.physics_Main.daq.RAW._lb0105._SFO-4._0002.data" \
  --maxEvents "${2}" \
  --outputAODFile "myAOD.pool.root" \
  --outputHISTFile "myHIST.pool.root" \
  --multithreaded "True" \
  --preExec "all:from AthenaMonitoring.DQMonFlags import DQMonFlags; DQMonFlags.doMonitoring=True; DQMonFlags.doNewMonitoring=True; DQMonFlags.doHLTMon=False; rec.doDetailedPerfMonMT = True;" \
  --autoConfiguration "everything" \
  --conditionsTag "all:CONDBR2-BLKPA-RUN2-09" \
  --geometryVersion "default:ATLAS-R2-2016-01-00-01" \
  --runNumber "357750" \
  --steering "doRAWtoALL"  > __stdout 2>&1

echo $? > __exitcode;

touch __done;
