#!/bin/bash

# ${1} = Number of threads
# ${2} = Number of events

touch __start;

TRF_ECHO=1 ATHENA_CORE_NUMBER="${1}" Reco_tf.py \
  --inputBSFile "/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/Tier0ChainTests/TCT_Run3/data22_13p6TeV.00429782.physics_Main.daq.RAW._lb0562._SFO-11._0001.data" \
  --maxEvents "${2}" \
  --outputAODFile "myAOD.pool.root" \
  --outputHISTFile "myHIST.pool.root" \
  --multithreaded "True" \
  --preExec "all:from RecExConfig.RecFlags import rec; rec.doZdc.set_Value_and_Lock(False); from AthenaConfiguration.AllConfigFlags import ConfigFlags; ConfigFlags.Trigger.triggerConfig=\"DB\"; ConfigFlags.DQ.Steering.HLT.doBjet=True; ConfigFlags.DQ.Steering.HLT.doInDet=True; ConfigFlags.DQ.Steering.HLT.doBphys=True; ConfigFlags.DQ.Steering.HLT.doCalo=True; ConfigFlags.DQ.Steering.HLT.doEgamma=True; ConfigFlags.DQ.Steering.HLT.doMET=True; ConfigFlags.DQ.Steering.HLT.doJet=True; ConfigFlags.DQ.Steering.HLT.doMinBias=True; ConfigFlags.DQ.Steering.HLT.doMuon=True; ConfigFlags.DQ.Steering.HLT.doTau=True; rec.doDetailedPerfMonMT = True;" \
  --autoConfiguration "everything" \
  --conditionsTag "all:CONDBR2-BLKPA-2022-06" \
  --geometryVersion "default:ATLAS-R3S-2021-03-00-00" \
  --runNumber "429782" \
  --steering "doRAWtoALL"  > __stdout 2>&1

echo $? > __exitcode;

touch __done;
