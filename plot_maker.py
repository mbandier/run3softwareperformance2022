#!/usr/bin/env python3

from definitions import base_run_dir, base_results_dir
import argparse
import os
import csv

# Execute the job
def execute_job( outdir = base_results_dir,
                 indir = base_results_dir,
                 fits = False,
                 prelim = False,
                 jobs = 'data18_RAWtoALL' ):

    # Gather the results
    results = {}
    for job in jobs.split(','):
        infile = f'{base_results_dir}/{job}.summary.csv'
        if not os.access( infile , os.R_OK ):
            print( f'Cannot access input file: {infile}; skipping' )
            continue
        with open( infile , 'r' ) as f:
            for line in f:
                if line.startswith('#') or len(line.split(','))<5:
                    continue
                my_job = line.split(',')[0]
                if my_job not in results:
                    results[my_job] = []
                # Add a tuple to the results with nthreads, events per second, and pss peak [GB]
                results[my_job] += [ ( int(line.split(',')[1]) , float(line.split(',')[3]) , float(line.split(',')[4]) ) ]

    # Import ROOT and basic graphics setup
    import ROOT
    ROOT.gROOT.SetBatch(True)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    # Dictionary of reasonably attractive marker / line colors and sizes
    colors = [ ROOT.kBlack , ROOT.kRed , ROOT.kBlue , ROOT.kGreen+1 , ROOT.kMagenta , ROOT.kOrange+2 , ROOT.kBlack , ROOT.kRed , ROOT.kBlue , ROOT.kGreen+1 ]
    markers = [         20 ,        21 ,         22 ,            23 ,            33 ,             34 ,          24 ,        25 ,         26 ,            32 ]
    sizes = [          1.5 ,       1.3 ,        1.4 ,           1.4 ,           1.7 ,            1.5 ,         1.4 ,       1.4 ,        1.4 ,           1.4 ]

    if len(jobs.split(','))>10:
        print('Warning: attempting to plot more than 10 jobs will result in repeated styles (and an ugly plot)')

    # Make a canvas - follow ATLAS standards
    canvas = ROOT.TCanvas("canvas","", 800, 600)
    canvas.SetTopMargin(0.02)
    canvas.SetRightMargin(0.04)
    canvas.SetBottomMargin(0.145)
    canvas.SetLeftMargin(0.12)
    canvas.SetBorderMode(0)
    canvas.cd()

    # Keep a dictionary of memory graphs keyed on job name
    memory_g = {}
    max_threads = 0
    max_mem = 0
    for job in results:
        memory_g[job] = ROOT.TGraph()
        for n,setup in enumerate(job):
            memory_g[job].SetPoint( n , job[0] , job[2] )
            if max_threads < job[0]:
                max_threads = job[0]
            if max_mem < job[2]:
                max_mem = job[2]

    # Create a simple frame on which to draw the memory plot
    # Leave a little buffer in the maximums
    frame_mem = ROOT.TH2F('frame_mem','',10,0.,max_threads*1.2,10,0.,max_mem*1.2)
    frame_mem.SetXTitle('Number of worker threads/processes')
    frame_mem.SetYTitle('Memory Usage [GB]')
    frame_mem.GetXaxis().SetTitleOffset(1.2)
    frame_mem.GetYaxis().SetTitleOffset(1.0)
    frame_mem.GetXaxis().SetTitleSize(0.055)
    frame_mem.GetXaxis().SetLabelSize(0.055)
    frame_mem.GetYaxis().SetTitleSize(0.055)
    frame_mem.GetYaxis().SetLabelSize(0.055)
    frame_mem.Draw()

    # Make and draw the fits - again a dictionary keyed on job name
    # Note that we always run the fits, the question is only if they're displayed or not
    memory_fits = {}
    for n,job in enumerate(memory_g):
        memory_g[job].Fit('pol1','R','',1,max_threads)
        if fits:
            memory_fits[job] = [ memory_g[job].GetFunction('pol1') ]
            memory_fits[job].SetLineWidth(2)
            memory_fits[job].SetLineStyle(2+n)
            memory_fits[job].SetLineColor( colors[n] )
            memory_fits[job].Draw('sameL')

    # Then draw the points on top
    for n,job in enumerate(memory_g):
        memory_g[job].SetMarkerColor( colors[n] )
        memory_g[job].SetLineColor( colors[n] )
        memory_g[job].SetMarkerColor( colors[n] )
        memory_g[job].SetLineWidth(2)
        memory_g[job].SetMarkerSize( sizes[n] )
        memory_g[job].SetMarkerStyle( markers[n] )
        memory_g[job].Draw('sameP')

    # Set up a legend - Generic placement for now
    memory_leg = ROOT.TLegend(0.16,0.67,0.56,0.90)
    memory_leg.SetFillStyle(0)
    memory_leg.SetLineColor(0)
    memory_leg.SetTextSize(0.035)

    # Add all the graphs to the legend with useful text
    for n,job in enumerate(memory_g):
        memory_leg.AddEntry( memory_g[job] , f'{job}: {round(memory_fits[job].GetParemeter(0),1)} GB + {round(memory_fits[job].GetParemeter(1),1)} GB/Thread','LP' )
        memory_leg.AddEntry( memory_g[job] , f'{job}: {round(memory_fits[job].GetParemeter(0),1)} GB + {round(memory_fits[job].GetParemeter(1),1)} GB/Thread','LP' )
    memory_leg.Draw()

    # Add ATLAS Interal or Preliminary to the canvas
    tex = ROOT.TLatex()
    tex.SetNDC()
    tex.SetTextFont( 42 )
    tex.SetTextSize( 0.060 )
    extra = 'Preliminary' if prelim else 'Internal'
    tex.DrawLatex( 0.18 , 0.905 , "#bf{#it{ATLAS}} "+extra )

    # Save the canvas with some useful file name
    extra += '_Fits' if fits else '_NoFits'
    canvas.SaveAs( f'{outdir}/Memory_{jobs.replace(",","_")}_{extra}.eps' ) 
    canvas.SaveAs( f'{outdir}/Memory_{jobs.replace(",","_")}_{extra}.eps' )


    # Now move on to throughput
    # Keep a dictionary of throughput graphs keyed on job name
    through_g = {}
    # We already did max_threads above, and it's the same results here
    max_tp = 0
    for job in results:
        through_g[job] = ROOT.TGraph()
        for n,setup in enumerate(job):
            through_g[job].SetPoint( n , job[0] , job[1] )
            if max_tp < job[1]:
                max_tp = job[1]

    # Create a simple frame on which to draw the memory plot
    # Leave a little buffer in the maximums
    frame_tp = ROOT.TH2F('frame_tp','',10,0.,max_threads*1.2,10,0.,max_tp*1.2)
    frame_tp.SetXTitle('Number of worker threads/processes')
    frame_tp.SetYTitle('Events per second (Total)')
    frame_tp.GetXaxis().SetTitleOffset(1.2)
    frame_tp.GetYaxis().SetTitleOffset(1.0)
    frame_tp.GetXaxis().SetTitleSize(0.055)
    frame_tp.GetXaxis().SetLabelSize(0.055)
    frame_tp.GetYaxis().SetTitleSize(0.055)
    frame_tp.GetYaxis().SetLabelSize(0.055)
    frame_tp.Draw()

    # Then draw the points on top - no fitting in this case!
    for n,job in enumerate(through_g):
        through_g[job].SetMarkerColor( colors[n] )
        through_g[job].SetLineColor( colors[n] )
        through_g[job].SetMarkerColor( colors[n] )
        through_g[job].SetLineWidth(2)
        through_g[job].SetMarkerSize( sizes[n] )
        through_g[job].SetMarkerStyle( markers[n] )
        through_g[job].Draw('sameP')

    # Set up a legend - Generic placement for now
    through_leg = ROOT.TLegend(0.16,0.67,0.56,0.90)
    through_leg.SetFillStyle(0)
    through_leg.SetLineColor(0)
    through_leg.SetTextSize(0.035)

    # Add all the graphs to the legend with useful text
    for n,job in enumerate(memory_g):
        memory_leg.AddEntry( memory_g[job] , f'{job}', 'P' )
    through_leg.Draw()

    # Add ATLAS Internal / Preliminary to the canvas as desired    
    tex = ROOT.TLatex()
    tex.SetNDC()
    tex.SetTextFont( 42 )
    tex.SetTextSize( 0.060 )
    extra = 'Preliminary' if prelim else 'Internal'
    tex.DrawLatex( 0.18 , 0.905 , "#bf{#it{ATLAS}} "+extra )

    # Save the canvas with some useful file name
    canvas.SaveAs( f'{outdir}/TotalThroughput_{jobs.replace(",","_")}.eps' )
    canvas.SaveAs( f'{outdir}/TotalThroughput_{jobs.replace(",","_")}.pdf' )


# Define the main function
def main():

    # Setup the job parameters here...
    parser = argparse.ArgumentParser(description = 'Basic Job Executer',
                                     add_help = True)

    parser.add_argument( '-j', '--jobs', action = 'store',
                         dest = 'jobs', default = 'data18_RAWtoALL',
                         help = 'The comma-separated list of jobs for which plots will be overlaid' )
    parser.add_argument( '-i', '--indir', action = 'store',
                         dest = 'indir', default = None,
                         help = 'The directory holding the input csv files (default base_results_dir)' )
    parser.add_argument( '-o', '--outdir', action = 'store',
                         dest = 'outdir', default = None,
                         help = 'The directory for saving the plots (default base_results_dir)' )
    parser.add_argument( '-f', '--fits', action = 'store',
                         dest = 'fits', default = False,
                         help = 'Overlay fits on the plots' )
    praser.add_argument( '-p', '--prelim', action = 'store',
                         dest = 'prelim', default = False,
                         help = 'Add Preliminary to plots (default False = Internal)' )
    parser.add_argument( '-v', '--version', action = 'version', version = '%(prog)s 0.0' )

    args = parser.parse_args()

    # Execute the job
    execute_job( outdir = args.outdir,
                 indir = args.indir,
                 fits = args.fits,
                 prelim = args.prelim,
                 jobs = args.jobs )

# When running this as a script
if '__main__' in __name__:
    main()
